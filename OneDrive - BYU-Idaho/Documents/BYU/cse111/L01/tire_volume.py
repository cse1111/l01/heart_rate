# Import math module so we can use pi
import math

# Get the user input
# Convert the user input to float() to get the numbers with decimal points
width = input("Enter the width of the tire in mm (ex 205): ") 
width = float(width)
ratio = input("Enter the aspect ratio of the tire (ex 60): ")
ratio = float(ratio)
diameter = input("Enter the diameter of the wheel in inches (ex 15): ")
diameter = float(diameter)

# Compute the volume of space inside the tire
# Solve the values inside the parenthesis first
width_and_ratio = width * ratio
diameter_and_constant = 2540 * diameter
# Compute the values outside the parenthesis and the values inside the parenthesis and divide with 10000000000
volume = math.pi * width**2 * ratio * (width_and_ratio + diameter_and_constant)
total_volume = volume / 10000000000
# Round the amount to nearest thousandths
total_volume = round(total_volume, 2)
# Print the output using f-string
print(f"The approximate volume is {total_volume} liters")